package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.google.android.gms.location.DetectedActivity;

/**
 * Created by james hermida on 10/27/17.
 */

public class NccMotionActivityMapper extends AbstractSensorDataMapper {

    static final String ACTIVITY_NOT_DETECTED = "0";
    static final String ACTIVITY_DETECTED = "1";

    @Override
    public String map(NccSensorData data) {
        NccMotionData md = (NccMotionData) data;
        clearStringBuilder();
        sb.append("Z,");                                                                                                            // Code
        sb.append(md.getActivityType() == DetectedActivity.STILL ? ACTIVITY_DETECTED : ACTIVITY_NOT_DETECTED); sb.append(",");      // S
        sb.append(md.getActivityType() == DetectedActivity.WALKING ? ACTIVITY_DETECTED : ACTIVITY_NOT_DETECTED); sb.append(",");    // W
        sb.append(md.getActivityType() == DetectedActivity.RUNNING ? ACTIVITY_DETECTED : ACTIVITY_NOT_DETECTED); sb.append(",");    // R
        sb.append(md.getActivityType() == DetectedActivity.IN_VEHICLE ? ACTIVITY_DETECTED : ACTIVITY_NOT_DETECTED); sb.append(","); // A
        sb.append(md.getActivityType() == DetectedActivity.ON_BICYCLE ? ACTIVITY_DETECTED : ACTIVITY_NOT_DETECTED); sb.append(","); // C
        sb.append(md.getActivityType() == DetectedActivity.UNKNOWN ? ACTIVITY_DETECTED : ACTIVITY_NOT_DETECTED); sb.append(",");    // U
        sb.append(String.valueOf(md.getConfidence())); sb.append(",");                                                              // Z
        sb.append("NA,");
        sb.append("NA,");
        sb.append(md.getTimestamp()); sb.append(",");                                                                               // UTCTime
        sb.append(getReadableTimestamp(md.getTimestamp()));                                                                         // Timestamp
        return sb.toString();
    }
}
