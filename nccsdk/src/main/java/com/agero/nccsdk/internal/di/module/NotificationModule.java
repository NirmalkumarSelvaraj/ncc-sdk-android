package com.agero.nccsdk.internal.di.module;

import android.content.Context;

import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.notification.NccNotificationManager;
import com.agero.nccsdk.internal.notification.NotificationManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by james hermida on 2/26/18.
 */

@Module
public class NotificationModule {

    @Provides
    @Singleton
    NotificationManager provideNotificationManager(Context context, DataManager dataManager) {
        return new NccNotificationManager(context, dataManager);
    }

}
