package com.agero.nccsdk.adt;

import android.content.Context;

import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.log.Timber;

public class AdtInactiveState extends AdtAbstractState {

    public AdtInactiveState(Context context) {
        super(context);
    }

    @Override
    public void onEnter() {
        Timber.d("Entered AdtInactiveState");
    }

    @Override
    public void onExit() {
        Timber.d("Exited AdtInactiveState");
    }

    @Override
    public void startMonitoring(StateMachine<AdtState> machine) {
        machine.setState(new AdtMonitoringState(applicationContext));
    }

    @Override
    public void stopMonitoring(StateMachine<AdtState> machine) {
        // DO NOTHING
    }

    @Override
    public void startDriving(StateMachine<AdtState> machine) {
        // DO NOTHING
    }

    @Override
    public void stopDriving(StateMachine<AdtState> machine) {
        // DO NOTHING
    }
}
