package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.adt.detection.start.model.WifiActivity;
import com.agero.nccsdk.adt.detection.start.model.WifiInfo;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccWifiData;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.log.Timber;

import javax.inject.Inject;

public class NccWifi extends NccAbstractBroadcastReceiverSensor {

    @Inject
    DataManager dataManager;

    private WifiActivity recentWifiActivity;
    private NccWifiData lastWifiData;
    private boolean hasFirstReadingBeenReceived = false;

    public NccWifi(Context context, NccConfig config) {
        super(context, NccWifi.class.getSimpleName(), NccSensorType.WIFI, config);
        NccSdk.getComponent().inject(this);
    }

    @Override
    String[] getActions() {
        return new String[]{
                "android.net.wifi.STATE_CHANGE"
        };
    }

    @Override
    boolean isLocalReceiver() {
        return false;
    }

    @Override
    public void onDataReceived(Context context, Intent intent) {
        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if (networkInfo == null) {
            Timber.w("Network info extra is null");
        } else {
            if (hasFirstReadingBeenReceived) {
                recentWifiActivity = dataManager.getRecentWifiActivity();
                long time = System.currentTimeMillis();
                String ssid = null;

                if (networkInfo.isConnected()) {
                    // Connected
                    ssid = getConnectedWifiSsid();

                    // Set connection info
                    setConnectionActivity(ssid, time);

                    // Save wifi info
                    dataManager.setRecentWifiActivity(recentWifiActivity);

                    postData(time, ssid, networkInfo);
                } else if (networkInfo.isConnectedOrConnecting()) {
                    // Connecting

                    // Do nothing here
                    // We check for this after isConnected() to avoid false positive connects
                    // and before !isConnected() to avoid false positive disconnects
                    // when the network is only connecting
                } else {
                    // Disconnected

                    // When the network is disconnected, the connected wifi ssid will be null or <unknown ssid>
                    // In this case, get the recent connection ssid, and set that as the disconnection ssid
                    if (recentWifiActivity.getConnection() != null && !StringUtils.isNullOrEmpty(recentWifiActivity.getConnection().getSsid())) {
                        ssid = recentWifiActivity.getConnection().getSsid();
                    }

                    // Set disconnection info
                    setDisconnectionActivity(ssid, time);

                    // Save wifi info
                    dataManager.setRecentWifiActivity(recentWifiActivity);

                    postData(time, ssid, networkInfo);
                }
            } else {
                // First reading should be ignored since the initial wifi state
                // is received when the sensor is first registered
                hasFirstReadingBeenReceived = true;
            }
        }
    }

    @SuppressWarnings({"MissingPermission"})
    private String getConnectedWifiSsid() {
        WifiManager wifiManager = (WifiManager) applicationContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        android.net.wifi.WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        ssid = ssid.replace("\"", ""); // some devices return the ssid wrapped with quotes
        return ssid;
    }

    private void setConnectionActivity(String ssid, long timestamp) {
        if (recentWifiActivity.getConnection() == null) {
            WifiInfo wifiInfo = new WifiInfo(ssid, timestamp);
            recentWifiActivity.setConnection(wifiInfo);
        } else {
            recentWifiActivity.getConnection().setSsid(ssid);
            recentWifiActivity.getConnection().setTimestamp(timestamp);
        }
    }

    private void setDisconnectionActivity(String ssid, long timestamp) {
        if (recentWifiActivity.getDisconnection() == null) {
            WifiInfo wifiInfo = new WifiInfo(ssid, timestamp);
            recentWifiActivity.setDisconnection(wifiInfo);
        } else {
            recentWifiActivity.getDisconnection().setSsid(ssid);
            recentWifiActivity.getDisconnection().setTimestamp(timestamp);
        }
    }

    private void postData(long time, String ssid, NetworkInfo networkInfo) {
        NccWifiData currentData = new NccWifiData(
                time,
                networkInfo.getType(),
                networkInfo.getTypeName(),
                ssid,
                networkInfo.isConnected(),
                networkInfo.isConnectedOrConnecting()
        );

        if (lastWifiData == null || !lastWifiData.isEqualTo(currentData)) {
            Timber.v("Wifi %s: %s", currentData.isConnected() ? "connected" : "disconnected", ssid);
            postData(currentData);
            lastWifiData = currentData;
        }
    }
}
