package com.agero.nccsdk;

import android.support.annotation.DrawableRes;
import android.support.annotation.RequiresApi;

import com.agero.nccsdk.adt.event.AdtEventListener;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.internal.data.memory.ClientCache;

/**
 * Entry-point interface definition to the SDK
 */
public interface NccSdkInterface extends ClientCache {
    /**
     * Initializes the SDK and validates the provided API key.
     *
     * This call makes a network request to authenticate the API key.
     * Upon success, the authenticated API key is cached. Subsequent calls
     * will not require a network request if the provided key matches the
     * cached key. If the authentication fails, the SDK will automatically
     * attempt to retry. Drive data will only be uploaded to the backend
     * if and only if authentication has succeeded.
     *
     * @param apiKey The API key used to authenticate the SDK
     * @param authenticateCallback The callback to be invoked when the initialization completes
     */
    void authenticate(String apiKey, AuthenticateCallback authenticateCallback);

    /**
     * Sets the user id that will be associated with the data collected by the SDK. The id must be unique within your app.
     *
     * @param userId The user id
     */
    void setUserId(String userId);

    /**
     * Gets the user id associated with the data collected by the SDK.
     *
     * @return A String user id or null if one was not previously set
     */
    String getUserId();

    /**
     * Starts location-based tracking.
     *
     * When tracking is enabled, location and sensor data is collected and uploaded
     * to the backend in real-time. To consume the location data, use
     * {@link NccSdkInterface#addTrackingListener(NccSensorListener)}.
     *
     */
    void startTracking();

    /**
     * Stops location-based tracking.
     */
    void stopTracking();

    /**
     * Adds a listener for location-based tracking.
     *
     * Data will only be received if {@link NccSdkInterface#startTracking()} is called prior
     * and a drive is in progress. Only {@link com.agero.nccsdk.domain.data.NccLocationData}
     * is provided to the listener. {@link NccSensorListener#onDataChanged(NccSensorType, NccSensorData)}
     * is invoked on the main thread.
     *
     * @param sensorListener The listener to be invoked when data is available
     * @throws NccException
     */
    void addTrackingListener(NccSensorListener sensorListener) throws NccException;

    /**
     * Removes a listener for location-based tracking.
     *
     * @param sensorListener The listener that no longer wants to receive callbacks for location-based tracking data
     * @throws NccException
     */
    void removeTrackingListener(NccSensorListener sensorListener) throws NccException;

    /**
     * Adds a listener for driving start and stop events. {@link AdtEventListener#onDrivingStart()}
     * and {@link AdtEventListener#onDrivingStop()} are invoked on the main thread.
     *
     * @param adtEventListener The listener to be invoked a new driving event is available
     */
    void addDrivingListener(AdtEventListener adtEventListener);

    /**
     * Removes a listener for driving start and stop events.
     *
     * @param adtEventListener The listener that no longer wants to be invoked when a new driving event is available
     */
    void removeDrivingListener(AdtEventListener adtEventListener);

    /**
     * Sets the notification that will be used for the foreground service that runs while collecting data.
     *
     * @param drawableId A resource ID in the application's package of the drawable to use.
     * @param contentTitle The title (first row) of the notification, in a standard notification.
     * @param contentText The text (second row) of the notification, in a standard notification.
     * @throws NccException
     */
    void setMonitoringNotification(@DrawableRes int drawableId, String contentTitle, String contentText) throws NccException;

    /**
     * Sets the callback to listen for when the ignoring battery optimizations setting is disabled. The callback is
     * only invoked when the app is promoted to the foreground. Consecutive calls will override the existing listener.
     *
     * @param ignoringBatteryOptimizationsCallback The callback to be invoked
     */
    @RequiresApi(value = 23)
    void setIgnoringBatteryOptimizationsCallback(IgnoringBatteryOptimizationsCallback ignoringBatteryOptimizationsCallback);

    /**
     * Removes the callback listening for the ignoring battery optimizations setting.
     */
    void clearIgnoringBatteryOptimizationsCallback();

    /**
     * Interface definition for a callback to be invoked when the authentication completes.
     */
    interface AuthenticateCallback {
        /**
         * Invoked when the sdk authentication succeeded.
         */
        void onSuccess();

        /**
         * Invoked when the sdk authentication failed.
         */
        void onFailure(Throwable throwable);
    }

    /**
     * Interface definition for a callback to be invoked when ignoring battery optimizations is disabled for the app.
     * This callback will only be invoked when the app is promoted to the foreground.
     */
    interface IgnoringBatteryOptimizationsCallback {
        /**
         * Invoked when ignoring battery optimizations is disabled for the app
         */
        void onDisabled();
    }
}
