package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccLinearAccelerometerData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccLinearAccelerometerMapperTest {

    private AbstractSensorDataMapper mapper = new NccLinearAccelerometerMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        float x = 0.98f;
        float y = 7.65f;
        float z = 4.32f;
        NccSensorData data = new NccLinearAccelerometerData(timeUtc, x, y, z);

        String expected = "A," + x + "," + y + "," + z + ",NA,NA,NA,NA,NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
