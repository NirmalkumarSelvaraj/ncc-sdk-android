package com.agero.nccsdk.ubi.collection.mapper;

import com.agero.nccsdk.domain.data.NccMagneticFieldUncalibratedData;
import com.agero.nccsdk.domain.data.NccSensorData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by james hermida on 12/5/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class NccMagneticFieldUncalibratedMapperTest {

    private AbstractSensorDataMapper mapper = new NccMagneticFieldUncalibratedMapper();

    @Test
    public void map() throws Exception {
        long timeUtc = 1315314000000L;
        String formattedDate = mapper.getReadableTimestamp(timeUtc);
        float bX = 5.46f;
        float bY = 6.57f;
        float bZ = 7.68f;
        int accuracy = 50;
        float iX = 2.13f;
        float iY = 3.24f;
        float iZ = 4.35f;
        NccSensorData data = new NccMagneticFieldUncalibratedData(timeUtc, bX, bY, bZ, iX, iY, iZ, accuracy);

        String expected = "RM," + bX + "," + bY + "," + bZ + "," + accuracy + "," + iX + "," + iY + "," + iZ + ",NA,NA," + timeUtc + "," + formattedDate;
        String actual = mapper.map(data);

        assertEquals(expected, actual);
    }
}
