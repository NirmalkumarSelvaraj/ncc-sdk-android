package com.agero.nccsdk.adt.detection.end.algo;

import android.content.Context;

import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.adt.AdtTriggerType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

/**
 * Created by james hermida on 11/28/17.
 */

@RunWith(PowerMockRunner.class)
public class LocationTimerDrivingEndStrategyTest {

    @Mock
    private Context mockContext;

    private long testTimeout = 1000;
    private float testSpeedThreshold = 100.0f;

    private LocationTimerDrivingEndStrategy locationTimerCollectionEndStrategy;

    @Before
    public void setUp() throws Exception {
        locationTimerCollectionEndStrategy = PowerMockito.spy(new LocationTimerDrivingEndStrategy(mockContext, testTimeout, testSpeedThreshold));
    }

    @Test
    @PrepareForTest({LocationTimerDrivingEndStrategy.class})
    public void evaluate_null_data() throws Exception {
        locationTimerCollectionEndStrategy.evaluate(null);

        PowerMockito.verifyPrivate(locationTimerCollectionEndStrategy, never()).invoke("scheduleStopCollectionBroadcast");
    }

    @Test
    @PrepareForTest({LocationTimerDrivingEndStrategy.class})
    public void evaluate_continues_collection() throws Exception {
        NccLocationData locationSpeedAboveThreshold = new NccLocationData(0, 0, 0, 0, 0, 0, testSpeedThreshold + 1.0f, 0);

        locationTimerCollectionEndStrategy.evaluate(locationSpeedAboveThreshold);

        PowerMockito.verifyPrivate(locationTimerCollectionEndStrategy).invoke("scheduleStopCollectionBroadcast");
    }

    @Test
    @PrepareForTest({LocationTimerDrivingEndStrategy.class})
    public void evaluate_ends_collection() throws Exception {
        NccLocationData locationSpeedBelowThreshold = new NccLocationData(0, 0, 0, 0, 0, 0, testSpeedThreshold - 1.0f, 0);

        locationTimerCollectionEndStrategy.evaluate(locationSpeedBelowThreshold);

        PowerMockito.verifyPrivate(locationTimerCollectionEndStrategy, never()).invoke("scheduleStopCollectionBroadcast");
    }

    @Test
    @PrepareForTest({LocationTimerDrivingEndStrategy.class})
    public void run() throws Exception {
        PowerMockito.doNothing().when(locationTimerCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));

        locationTimerCollectionEndStrategy.run();
        verify(locationTimerCollectionEndStrategy).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_TIMER);
    }
}
